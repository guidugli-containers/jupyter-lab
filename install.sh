#!/bin/bash

SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

mkdir -p $HOME/.local/share/icons &>/dev/null

cp -f $SCRIPT_DIR/icons/jupyter-logo-transparent.png $HOME/.local/share/icons/

cp -f $SCRIPT_DIR/resources/run.sh $HOME/.local/bin/jupyter_podman_run.sh

chmod 700 $HOME/.local/bin/jupyter_podman_run.sh

cat << EOF > $HOME/.local/share/applications/jupyter_podman.desktop
[Desktop Entry]
Type=Application
Encoding=UTF-8
Name=Jupyter Lab Podman
Comment=Development
Icon=$HOME/.local/share/icons/jupyter-logo-transparent.png
Exec=$HOME/.local/bin/jupyter_podman_run.sh
Terminal=false
Categories=Tags;Describing;Application
EOF

chmod 600 $HOME/.local/share/icons/jupyter-logo-transparent.png $HOME/.local/share/applications/jupyter_podman.desktop

